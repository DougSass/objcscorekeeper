//
//  ViewController.h
//  ObjCScoreKeeper
//
//  Created by Douglas Sass on 1/31/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *team1Lbl;
@property (weak, nonatomic) IBOutlet UIStepper *team1Stepper;

@property (weak, nonatomic) IBOutlet UILabel *team2Lbl;
@property (weak, nonatomic) IBOutlet UIStepper *team2Stepper;


@end

