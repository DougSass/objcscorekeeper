//
//  AppDelegate.h
//  ObjCScoreKeeper
//
//  Created by Douglas Sass on 1/31/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

