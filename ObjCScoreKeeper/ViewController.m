//
//  ViewController.m
//  ObjCScoreKeeper
//
//  Created by Douglas Sass on 1/31/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)Team1Stepper:(UIStepper *)sender {
    self.team1Lbl.text = [NSString stringWithFormat:@"%.f", sender.value];
}

- (IBAction)Team2Stepper:(UIStepper *)sender {
    self.team2Lbl.text = [NSString stringWithFormat:@"%.f", sender.value];
}


- (IBAction)resetButtonPressed:(UIButton *)sender {
    self.team1Stepper.value = 0;
    self.team2Stepper.value = 0;
    self.team1Lbl.text = [NSString stringWithFormat:@"%.f", self.team1Stepper.value];
    self.team2Lbl.text = [NSString stringWithFormat:@"%.f", self.team2Stepper.value];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
